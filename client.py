#!/usr/bin/python3 -W ignore
#Import modules
import ipfsapi, wget, os, json, sys #To hide warnings use the cmd : `python -W ignore app.py`

#ipfs import and configure
api = ipfsapi.connect('127.0.0.1', 5001)

#Get arguments
opt = sys.argv[1]

#Import dictionary
f = open('dict.json', 'r')
packet = f.read()
d = json.loads(packet)

#Upload
if opt == 'u':
    res = api.add(sys.argv[2])
    print(res)
    api.cat(res['Hash'])
    url = 'Your file is available on http://127.0.0.1:8080/ipfs/' + res['Hash']
    print(url)

    #Send to the list
    d[sys.argv[3]] = res['Hash']
    print(d)

    #Post dictionary
    packet = json.dumps(d)
    #post to address here, the server will append the dictionary to its own

    #Send it to file
    packet = json.dumps(d)
    f = open('dict.json', 'w')
    f.write(packet)
    f.close()

#Download
if opt == 'd':
    url = 'http://127.0.0.1:8080/ipfs/' + sys.argv[2]
    print(url)
    wget.download(url)
    print('Your file has been downloaded!')

if opt == 's':
    print('Looking for ' + sys.argv[2] + '...')
    try:
        print('Downloading from the remote...')
        wget.download('127.0.0.1:4444/dict.json')
        f = open('dict.json', 'r')
        packet = f.read()
        f.close()
        d = json.loads(packet)
    except:
        print('The sync with the remote gone wrong. Ignored.')
    try:
        print('URL found! ' + d[sys.argv[2]])
        url = 'http://127.0.0.1:8080/ipfs/' + d[sys.argv[2]]
        wget.download(url)
        os.rename(d[sys.argv[2]], sys.argv[2])
        print('')
        print('File downloaded!')
    except:
        print('Uh Oh, your file is not found : /')

if opt == 'h':
    print("There are 3 options available:")
    print("u FILE NAME >>> Upload a file with a name")
    print("d HASH >>> Download from hash")
    print("s NAME >>> Download from name")
